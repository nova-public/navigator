# Nova Navigator

The Nova Navigator is an open source tool for bookmarking and sharing surface locations in Star Citizen. At the moment the game has no way to store or share surface locations. Every planet and moon *does* have Orbital Markers (OM 1-6) that are geo stationary and can be used for navigation. This gives us a way to store our location by recording the distance to the 3 closest OM points. That works, but finding your way back using those distances can be frustrating.

There are existing projects that do the same as NovaNav does (Check the History below), but we decided to build our own and release it in a way everyone could benefit. Feel free to copy, modify and host your own version this tool. This is a static site so it should be easy to implement in an existing project.

### Live Examples

- [Daymar - Constellation Wreck](https://nova-public.gitlab.io/navigator/?title=Daymar%20-%20Constellation%20Wreck&distance=430&om2=486.5&om3=460.7&om5=168.4&size=295.2)
- [Clio - Starfarer Wreck](https://nova-public.gitlab.io/navigator/?title=Clio%20-%20Starfarer%20Wreck&om1=294.9&om3=208.6&om5=397.8&size=240&distance=352.2)
- [Lyria - Paradise Cove](https://nova-public.gitlab.io/navigator/?title=Lyria%20-%20Paradise%20Cove&om2=347.8&om3=166.6&om6=316.6&size=223&distance=328.2)
- [Magda - Caterpillar Wreck](https://nova-public.gitlab.io/navigator/?title=Magda%20-%20Caterpillar%20Wreck&om1=550.2&om3=516.8&om5=205.3&size=341&distance=494.8)
- Create your own at: https://nova-public.gitlab.io/navigator/

### Navigation example
![Navigation example](image/example-screenshot-1.png "Navigation example")

### Editor example
![Editor example](image/example-screenshot-2.png "Editor example")

## History

Back in 2019(-ish?) I was thinking about calculating routes to a surface location that you could follow by always traveling to a visible point. I started looking around and found that [PandaBot](https://robertsspaceindustries.com/citizens/Pandabot) just released something similar: https://pandabotz.github.io/sc-route-calc/. Awesome! I was working on the Nova Trade Calculator at that time and I had no time to spend on other projects, so I started to use that tool for my navigation needs.

A while later I had a discord chat with [LordSkippy](https://robertsspaceindustries.com/citizens/LordSkippy) and he was also hooked on the idea to create something like that tool. He started working on https://verseguide.com. That turned out great (serious, check it out) But was a bit too elaborate for my taste. I just wanted a compact tool to bookmark (and maybe share) locations, preferably using my phone.

That is why I decided ot start working on this project. At the time I was playing around with Threejs and decided to use that for the visualisation. An added benefit was that I could use their 3D logic to keep my math as simple as possible and debug my logic visually. I came up with a pretty solid algorythm and we have been using it for a few months now.


-- [SandovalBurrows](https://robertsspaceindustries.com/citizens/SandovalBurrows) @ [NOVA Intergalactic](https://novaintergalactic.com/) 

To contact me on Discord: SandovalBurrows#4256

## License

This project is licensed under CC BY 4.0.

**You are free to:**

- Share — copy and redistribute the material in any medium or format.
- Adapt — remix, transform, and build upon the material for any purpose, even commercially.

**Under the following terms:**

- Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
